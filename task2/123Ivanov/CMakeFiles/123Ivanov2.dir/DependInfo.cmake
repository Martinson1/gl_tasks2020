# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/data/DIHT/Fourth_Course/CompoGraphics/opengl_tasks_2020/task2/123Ivanov/gl1_SimpleTriangle.cpp" "/data/DIHT/Fourth_Course/CompoGraphics/opengl_tasks_2020/task2/123Ivanov/CMakeFiles/123Ivanov2.dir/gl1_SimpleTriangle.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_STATIC"
  "GLFW_DLL"
  "GLM_FORCE_DEPTH_ZERO_TO_ONE"
  "GLM_FORCE_RADIANS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "external/glew-1.13.0/include"
  "external/GLM"
  "external/SOIL/src/SOIL2"
  "external/Assimp/include"
  "external/GLFW/include"
  "external/imgui"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/data/DIHT/Fourth_Course/CompoGraphics/opengl_tasks_2020/external/glew-1.13.0/build/cmake/CMakeFiles/glew_s.dir/DependInfo.cmake"
  "/data/DIHT/Fourth_Course/CompoGraphics/opengl_tasks_2020/external/SOIL/CMakeFiles/soil.dir/DependInfo.cmake"
  "/data/DIHT/Fourth_Course/CompoGraphics/opengl_tasks_2020/external/Assimp/code/CMakeFiles/assimp.dir/DependInfo.cmake"
  "/data/DIHT/Fourth_Course/CompoGraphics/opengl_tasks_2020/external/imgui/CMakeFiles/imgui.dir/DependInfo.cmake"
  "/data/DIHT/Fourth_Course/CompoGraphics/opengl_tasks_2020/external/Assimp/contrib/irrXML/CMakeFiles/IrrXML.dir/DependInfo.cmake"
  "/data/DIHT/Fourth_Course/CompoGraphics/opengl_tasks_2020/external/GLFW/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
